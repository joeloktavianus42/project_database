﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Entities.Entities.PostGre
{
    [Table("buku")]
    public partial class Buku
    {
        [Key]
        [Column("kodebuku")]
        [StringLength(3)]
        public string Kodebuku { get; set; }
        [Column("judulbuku")]
        [StringLength(20)]
        public string Judulbuku { get; set; }
        [Column("namapengarang")]
        [StringLength(20)]
        public string Namapengarang { get; set; }
    }
}
