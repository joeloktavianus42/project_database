﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication1.Entities.Entities.PostGre
{
    public partial class PostGreDbContext : DbContext
    {

        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Buku> Buku { get; set; }
        public virtual DbSet<Tbbarang> Tbbarang { get; set; }
        public virtual DbSet<Tbbenda> Tbbenda { get; set; }

        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Buku>(entity =>
            {
                entity.HasKey(e => e.Kodebuku)
                    .HasName("buku_pkey");

                entity.Property(e => e.Kodebuku).IsFixedLength();
            });

            modelBuilder.Entity<Tbbarang>(entity =>
            {
                entity.HasKey(e => e.Kodebarang)
                    .HasName("tbbarang_pkey");

                entity.Property(e => e.Kodebarang).IsFixedLength();
            });

            modelBuilder.Entity<Tbbenda>(entity =>
            {
                entity.HasKey(e => e.Kodebenda)
                    .HasName("tbbenda_pkey");

                entity.Property(e => e.Kodebenda).IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
